

import com.oleksandr.Locker;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class LockerTest {

    @Test
    public void testLocker() {
        Locker locker = new Locker();

        final int distract = 5;
        final int multiplyBy = 10;
        int add = 7;

        Runnable distractor = () -> locker.distract(distract);
        Runnable multiplier = () -> locker.multiplyBy(multiplyBy);
        Runnable added = () -> locker.add(add);

        ExecutorService executor = Executors.newFixedThreadPool(3);
        executor.submit(distractor);
        executor.submit(multiplier);
        executor.submit(added);

        executor.shutdown();

        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

}
