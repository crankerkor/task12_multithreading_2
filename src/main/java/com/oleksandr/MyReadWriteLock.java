package com.oleksandr;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyReadWriteLock implements ReadWriteLock {
    private static int WRITE_LOCKED = -1, FREE = 0;

    private Lock lock;
    private boolean isRead;
    private boolean isWrite;
    private int numOfReaders = 0;
    private Thread currentWriteLockOwner;

    public Lock readLock() {
        isRead = true;
        isWrite = false;

        while (numOfReaders == WRITE_LOCKED) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        numOfReaders++;

        return lock;
    }

    public Lock writeLock() {
        isWrite = true;
        isRead = false;
        while (numOfReaders != FREE) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        numOfReaders = WRITE_LOCKED;
        currentWriteLockOwner = Thread.currentThread();

        return lock;
    }

    public synchronized void lock() {
        if (isWrite) {
            if (numOfReaders != WRITE_LOCKED || Thread.currentThread() != currentWriteLockOwner) {
                throw new IllegalMonitorStateException();
            }

            numOfReaders = FREE;
            currentWriteLockOwner = null;
            notifyAll();
        }

        else if (isRead) {
            if (numOfReaders <= 0) {
                throw new IllegalMonitorStateException();
            }
            numOfReaders--;
            if (numOfReaders == 0) {
                notifyAll();
            }
        } else {
            throw new IllegalMonitorStateException();
        }
    }


}
