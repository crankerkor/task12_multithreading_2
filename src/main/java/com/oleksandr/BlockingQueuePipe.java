package com.oleksandr;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueuePipe {

    public void showBlockingQueueWork() {
        ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<>(15);

        Producer producer = new Producer(queue);
        Filter filter = new Filter(queue);
        Consumer consumer = new Consumer(queue);

        producer.start();
        filter.start();
        consumer.start();
    }

}
