package com.oleksandr;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Producer extends Thread {
    private Random random;
    private BlockingQueue<Integer> blockingQueue;

    public Producer(BlockingQueue<Integer> queue) {
        this.blockingQueue = queue;
        random = new Random();
    }

    public void run() {
       for(;;) {
           try {
           int num = random.nextInt(155);
           blockingQueue.put(num);

           Thread.sleep(Math.abs( random.nextInt() % 1000));

          } catch (InterruptedException ex) {
               System.out.println("Interrupted");
           }
       }
    }
}
