package com.oleksandr;

import java.util.concurrent.BlockingQueue;

public class Filter extends Thread {
  private BlockingQueue<Integer> blockingQueue;
  private int count;

  public Filter(BlockingQueue<Integer> queue) {
    this.blockingQueue = queue;
  }

  public void run() {
    for (; ; ) {
      try {
        int x = blockingQueue.take();
        count++;
        x += count;

        if (count != 0) {
          blockingQueue.put(x);
        }

      } catch (NullPointerException | InterruptedException ex) {
        ex.printStackTrace();
      }
    }
  }
}
