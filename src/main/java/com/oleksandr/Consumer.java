package com.oleksandr;

import java.util.concurrent.BlockingQueue;

public class Consumer extends Thread {
    private BlockingQueue<Integer> blockingQueue;

    public Consumer(BlockingQueue<Integer> queue) {
        this.blockingQueue = queue;
    }

  public void run() {
    for (; ; ) {
        try {
          int x = blockingQueue.take();

          if (x > 0) {
            System.out.println(x);
          }
      } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
  }
}
