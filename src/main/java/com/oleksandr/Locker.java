package com.oleksandr;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Locker {
    private Lock lock = new ReentrantLock();

    private static int number = 0;

    public void add(int num) {
        lock.lock();
        try {
            number += num;
            System.out.println("add");
            System.out.println("Number: " + number);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted");
            }
        } finally {
            lock.unlock();
        }
    }

    public void multiplyBy(int num) {
        lock.lock();
        try {
            number *= num;
            System.out.println("multiply");
            System.out.println("Number: " + number);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted");
            }
        } finally {
            lock.unlock();
        }
    }

    public void distract(int num) {
        lock.lock();
        try {
            number -= num;
            System.out.println("distract");
            System.out.println("Number: " + number);
            try {
                Thread.sleep(4000);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted");
            }
        } finally {
            lock.unlock();
        }
    }
}
